import np as np
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LogisticRegression as lr
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn import svm
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import RFECV

def CreateSubmission(df):
    selection_df = df[['PassengerId','Survived']]
    selection_df.to_csv('Prediction_submission.csv', index = False)

# Read training data
db = pd.read_csv('train.csv')

# Show info about the dataset
print(db.info())
print(db.describe())

# Drop several things: Cabin, as there are many nans; All rows where at least 1 NaN is present
db = db.drop(['Cabin'], axis=1)
db = db.dropna()

# Create dummy variables for male (0) and female (1)
db.loc[db['Sex']=='male','Sex']=0
db.loc[db['Sex']=='female','Sex']=1

# Create training set parameters
y = db['Survived']
x = db.drop(['Survived'],axis=1)
x = x.loc[:,['Fare','Pclass','Sex','Age']]

# Train and test models models
# Logistic regression
clf = lr(random_state=0)
print('Logistic regression: ' + str(cross_val_score(clf,x,y, cv=5).mean()))

clf = AdaBoostClassifier(n_estimators=1000)
print('AdaBoost: ' + str(cross_val_score(clf,x,y, cv=5).mean()))

clf = svm.SVC(kernel='linear')
print('SVM: ' + str(cross_val_score(clf,x,y, cv=5).mean()))

clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes = (5,1))
print('Neural network: ' + str(cross_val_score(clf,x,y, cv=5).mean()))

clf.fit(x,y)

# --------------------- Test set ------------------------------ #
# Read test data
dbtest = pd.read_csv('test.csv')

#Create dummy variables for sex
dbtest.loc[dbtest['Sex']=='male','Sex']=0
dbtest.loc[dbtest['Sex']=='female','Sex']=1

# Replace NaNs in Age and Fare with the median
dbtest.loc[dbtest['Age'].isnull(),'Age']=dbtest['Age'].median()
dbtest.loc[dbtest['Fare'].isnull(),'Fare']=dbtest['Fare'].median()

# Create x for the test set
xtest = dbtest.loc[:,['Fare','Pclass','Sex','Age']]

# Predict the survived people and add them to the DB
dbtest.insert(len(list(dbtest.columns)),'Survived',clf.predict(xtest))

# Create excelfile for submission
CreateSubmission(dbtest)
